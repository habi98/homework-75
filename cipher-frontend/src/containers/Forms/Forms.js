import React, {Component} from 'react';
import {Button, Col, Container, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {postDecode, postEncode, encodeHandler, decodeHandler, changePassword} from "../../store/actions";

class Forms extends Component {

    encodeHandler = () => {
        this.props.postToEncode({password: this.props.password, message: this.props.encode})
    };

    decodeHandler = () => {
        console.log(this.props.password);
        this.props.postToDecode({password: this.props.password, message: this.props.decode})
    };

    render() {
        return (
            <Container className="p-4">
                <Form className="mt-4">
                <FormGroup row>
                    <Label for="exampleText" sm={2}>Decoded message </Label>
                    <Col sm={10}>
                        <Input value={this.props.encode} onChange={this.props.encodeHandler} type="textarea" name="message" id="exampleText" />
                    </Col>
                </FormGroup>

                    <FormGroup row>
                        <Label for="examplePassword" sm={2}>Password</Label>
                        <Col sm={8}>
                            <Input value={this.props.password} onChange={this.props.changePassword} type="text" name="password"/>
                        </Col>
                        <Col sm={2}>
                            <Button className="mr-3" onClick={this.encodeHandler} style={{fontSize: '20px'}}>↑</Button>
                            <Button className="ml-3" onClick={this.decodeHandler} style={{fontSize: '20px'}}>↓</Button>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label for="exampleText" sm={2}>Encoded message</Label>
                        <Col sm={10}>
                            <Input type="textarea" onChange={this.props.decodeHandler} value={this.props.decode} name="encode" id="exampleText" />
                        </Col>
                    </FormGroup>
                </Form>
            </Container>

        );
    }
}

const mapStateToProps = state => ({
     encode: state.encode,
    decode: state.decode,
    password: state.password
});

const mapDispatchToProps = dispatch => ({
    postToEncode: encode => dispatch(postEncode(encode)),
    postToDecode: data => dispatch(postDecode(data)),
    encodeHandler: (event) => dispatch(encodeHandler(event)),
    decodeHandler: (event) => dispatch(decodeHandler(event)),
    changePassword: (event) => dispatch(changePassword(event))
});


export default  connect(mapStateToProps, mapDispatchToProps)(Forms);