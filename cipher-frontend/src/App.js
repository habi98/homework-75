import React, { Component } from 'react';
import './App.css';
import Forms from "./containers/Forms/Forms";

class App extends Component {
  render() {
    return (
       <Forms/>
    );
  }
}

export default App;
