import {POST_ENCODE_SUCCESS, PASSWORD_CHANGE, DECODE_HANDLER, ENCODE_HANDLER, POST_DECODE_SUCCESS} from "./actions";



const initialState = {
    encode: '',
    decode: '',
    password: ''
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case POST_ENCODE_SUCCESS:
            return {...state, decode: action.encode};
        case ENCODE_HANDLER:
            return {...state, encode: action.encode};
        case DECODE_HANDLER:
            return {...state, decode: action.decode};
        case PASSWORD_CHANGE:
            return {...state, password: action.password};
        case POST_DECODE_SUCCESS:
            console.log(action.decode);
            return {...state, encode: action.decode};
        default:
            return state
    }

};


export default reducer