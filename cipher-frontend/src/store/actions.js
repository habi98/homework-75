import axios from '../axios-cipher'

export const POST_ENCODE_SUCCESS = 'POST_ENCODE_SUCCESS';
export const ENCODE_HANDLER = 'ENCODE_HANDLER';
export const DECODE_HANDLER = 'DECODE_HANDLER';
export const PASSWORD_CHANGE = 'PASSWORD_CHANGE';
export const POST_DECODE_SUCCESS = 'POST_DECODE_SUCCESS';

export const postToEncode = (encode) => ({type: POST_ENCODE_SUCCESS, encode});
export const postToDecode = (decode) => ({type: POST_DECODE_SUCCESS, decode})

export const changePassword = (event) => {
    return {type: PASSWORD_CHANGE, password: event.target.value};
};

export const encodeHandler = (event) => {
    return {type: ENCODE_HANDLER, encode: event.target.value}
};

export const decodeHandler = (event) => {
    return {type: DECODE_HANDLER, decode: event.target.value};
};

export const postEncode = encode => {
    return dispatch => {
        axios.post('encode', encode).then(response => {
            dispatch(postToEncode(response.data.encode))
        })
    }
};
export const postDecode = data => {
    console.log(data);
    return dispatch => {
        axios.post('dencode', data).then(response => {
            console.log(response);
            dispatch(postToDecode(response.data.dencode))
        })
    }
};
