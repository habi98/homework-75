const express = require('express');
const router = express.Router();
const Vigenere = require('caesar-salad').Vigenere;


router.post('/encode', (req, res) => {
    console.log(req.body);
    const encode = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    res.send({encode})
});


router.post('/dencode', (req, res) => {
    const dencode = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    res.send({dencode})
});


module.exports  = router;