const express = require('express');
const app = express();
const cors = require('cors');
const cipher = require('./app/cipher');
const port = 8000;
app.use(express.json());
app.use(cors());


app.use('/', cipher);

app.listen(port, () => {
    console.log('We are live on ' + port)
});